import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { applyMiddleware } from "redux";
import thunk from "redux-thunk";
import reducer from "./reducers";
import * as serviceWorker from "./serviceWorker";

import "./Style.scss";
import Main from "./components/Main";
const store = createStore(reducer, applyMiddleware(thunk));

ReactDOM.render(
  <Provider store={store}>
    <Main />
  </Provider>,

  document.getElementById("root")
);

serviceWorker.register();
