import React from "react";
import { connect } from "react-redux";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";
const SongDetail = ({ song }) => {
  return (
    <>
      {song ? (
        <Link style={{ textDecoration: "none" }} to={`/detail/${song.id}`}>
          <Card style={{ maxWidth: "480px" }}>
            <CardActionArea>
              <CardMedia
                component="img"
                alt={song.title}
                height="260"
                image={song.url}
                title={song.title}
              />
              <CardContent>
                <Typography
                  color="primary"
                  gutterBottom
                  variant="h5"
                  component="h2"
                >
                  {song.title}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  {song.desc}
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Link>
      ) : (
        <span style={{ color: "white" }}>no song selected</span>
      )}
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    song: state.song,
  };
};

export default connect(mapStateToProps)(SongDetail);
