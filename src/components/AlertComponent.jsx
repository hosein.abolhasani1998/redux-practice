import React from "react";
import Snackbar from "@material-ui/core/Snackbar";

const AlertComponent = (props) => {
  return (
    <Snackbar
      severity={props.severity}
      anchorOrigin={{
        vertical: "top",
        horizontal: "left",
      }}
      open={props.snack}
      onClose={props.handleSnackClose}
      autoHideDuration={2500}
    >
      {props.children}
    </Snackbar>
  );
};

export default AlertComponent;
