import React, { useState } from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import SkipPreviousIcon from "@material-ui/icons/SkipPrevious";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import SkipNextIcon from "@material-ui/icons/SkipNext";
import { connect } from "react-redux";

const DetailPage = (props) => {
  const [desc, setDesc] = useState(false);
  return (
    <>
      <div className="wrapper">
        <Card className="music-card">
          <div className="details">
            <CardContent className="content">
              <Typography component="h6">{props.song.title}</Typography>
              <Typography variant="subtitle1" color="textSecondary">
                {props.song.duration}
              </Typography>
            </CardContent>

            <div className="controls">
              <IconButton>
                <SkipPreviousIcon />
              </IconButton>
              <IconButton onClick={() => setDesc(true)}>
                <PlayArrowIcon />
              </IconButton>
              <IconButton>
                <SkipNextIcon />
              </IconButton>
            </div>
          </div>
          <CardMedia
            className="cover-music"
            image={props.song.url}
            title={props.song.title}
          />
        </Card>
      </div>
      <div className="desc-section">
        {desc ? (
          <Typography variant="body1" display="block">
            {props.song.desc}
          </Typography>
        ) : null}
      </div>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    song: state.song,
  };
};

export default connect(mapStateToProps)(DetailPage);
