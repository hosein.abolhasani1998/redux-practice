import React, { lazy, Suspense } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./Home";
import Header from "./Header";
import LinearProgress from "@material-ui/core/LinearProgress";

const DetailPage = lazy(() => import("./DetailPage"));

const Main = () => {
  return (
    <Router>
      <Suspense
        fallback={
          <LinearProgress
            color="secondary"
            style={{
              background: "linear-gradient(to right, #ff9966, #ff5e62)",
            }}
          />
        }
      >
        <Header />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/detail/:id" component={DetailPage} />
        </Switch>
      </Suspense>
    </Router>
  );
};

export default Main;
