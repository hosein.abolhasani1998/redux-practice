import React from "react";
import SongList from "./SongList";
import SongDetail from "./SongDetail";
import AddSong from "./AddSong";
import Grid from "@material-ui/core/Grid";

const Home = () => {
  return (
    <Grid className="container" container>
      <Grid item xs={12} lg={6} md={6}>
        <SongList />
      </Grid>
      <Grid className="detail-col" item xs={12} lg={6} md={6}>
        <SongDetail />
      </Grid>
      <AddSong />
    </Grid>
  );
};

export default Home;
