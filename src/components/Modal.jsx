import React, { useState } from "react";
import { createPortal } from "react-dom";
import { addSong } from "../actions";
import { connect } from "react-redux";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import Button from "@material-ui/core/Button";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import Typography from "@material-ui/core/Typography";
import Alert from "@material-ui/lab/Alert";
import AlertComponent from "./AlertComponent";

const Modal = (props) => {
  const [snack, showSnack] = useState(false);
  const [title, setTitle] = useState("");
  const [duration, setDuration] = useState("");
  const [url, setUrl] = useState("");
  const [desc, setDesc] = useState("");

  const handleSnackClose = () => {
    showSnack(false);
  };

  const onClickHandler = () => {
    if (title === "" || duration === "") {
      showSnack(true);
    } else {
      const newSong = {
        title,
        duration,
        url,
        desc,
      };
      props.addSong(newSong);

      setTitle("");
      setDuration("");
      setUrl("");
      setDesc("");
      props.close();
    }
  };

  return createPortal(
    <Dialog
      onClose={props.close}
      aria-labelledby="customized-dialog-title"
      open={props.open}
    >
      <DialogTitle>
        <Typography color="textSecondary"> Add a Song</Typography>
      </DialogTitle>
      <DialogContent>
        <FormControl>
          <FormControl>
            <InputLabel htmlFor="title">Song Title</InputLabel>
            <Input
              id="title"
              aria-describedby="helper-tilte"
              autoComplete="off"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
          </FormControl>
          <FormControl className="form-item">
            <InputLabel htmlFor="duration">Duration</InputLabel>
            <Input
              id="duration"
              aria-describedby="helper-duration"
              autoComplete="off"
              value={duration}
              onChange={(e) => setDuration(e.target.value)}
            />
          </FormControl>
          <FormControl className="form-item">
            <InputLabel htmlFor="url">Url</InputLabel>
            <Input
              id="url"
              type="url"
              aria-describedby="helper-url"
              autoComplete="off"
              value={url}
              onChange={(e) => setUrl(e.target.value)}
            />
          </FormControl>
          <FormControl className="form-item">
            <InputLabel htmlFor="desc">Description</InputLabel>
            <Input
              id="desc"
              aria-describedby="helper-desc"
              value={desc}
              autoComplete="off"
              onChange={(e) => setDesc(e.target.value)}
            />
          </FormControl>
        </FormControl>
      </DialogContent>
      <DialogActions>
        <Button
          variant="contained"
          fullWidth="true"
          style={{
            background: "linear-gradient(to right, #ff9966, #ff5e62)",
            color: "white",
          }}
          onClick={onClickHandler}
        >
          Add
        </Button>
      </DialogActions>

      <AlertComponent
        snack={snack}
        handleSnackClose={handleSnackClose}
        message="Please Fill the Inputs"
        severity="error"
      >
        <Alert
          style={{
            background: "linear-gradient(to right, #ff9966, #ff5e62)",
            color: "white",
          }}
          severity="error"
        >
          Please Fill The Inputs !
        </Alert>
      </AlertComponent>
    </Dialog>,
    document.getElementById("modal")
  );
};

export default connect(null, { addSong })(Modal);
