import React, { useState, lazy } from "react";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";

const Modal = lazy(() => import("./Modal"));

const AddSong = () => {
  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <>
      <Fab
        onClick={handleOpen}
        className="fab-btn"
        size="small"
        aria-label="add"
      >
        <AddIcon className="add" />
      </Fab>
      <Modal open={open} close={handleClose} />
    </>
  );
};

export default AddSong;
