import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import IconButton from "@material-ui/core/IconButton";
import Avatar from "@material-ui/core/Avatar";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import CircularProgress from "@material-ui/core/CircularProgress";
import AudioTrack from "@material-ui/icons/Audiotrack";
import Launch from "@material-ui/icons/Launch";
import { selectSong, getSongs } from "../actions";
const SongList = ({ songs, selectSong, getSongs }) => {
  useEffect(() => {
    getSongs();
    // eslint-disable-next-line
  }, []);

  const renderList = () => {
    if (songs.length) {
      return songs.map((song) => {
        return (
          <ListItem key={song.id}>
            <ListItemAvatar>
              <Avatar
                style={{
                  background: "linear-gradient(to right, #ff9966, #ff5e62)",
                }}
              >
                <AudioTrack />
              </Avatar>
            </ListItemAvatar>
            <ListItemText
              style={{ paddingTop: "1rem ", color: "white" }}
              primary={song.title}
              secondary={song.duration}
            />
            <ListItemSecondaryAction>
              <IconButton
                onClick={() => selectSong(song)}
                edge="end"
                aria-label="select"
              >
                <Launch className=" select" />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        );
      });
    }
    return <CircularProgress />;
  };
  return (
    <>
      <List>{renderList()}</List>
    </>
  );
};
SongList.propTypes = {
  songs: PropTypes.array.isRequired,
  getSongs: PropTypes.func.isRequired,
  selectSong: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  return {
    songs: state.songs,
  };
};
export default connect(mapStateToProps, { selectSong, getSongs })(SongList);
