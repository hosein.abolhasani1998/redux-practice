import React from "react";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { Link } from "react-router-dom";
const SideDrawer = (props) => {
  return (
    <Drawer anchor="top" open={props.open} onClose={() => props.onClose(false)}>
      <List component="nav" style={{ background: "#ff9966", color: "white" }}>
        <Link
          onClick={() => props.onClose(false)}
          style={{ textDecoration: "none", color: "white" }}
          to="/"
        >
          <ListItem button>Home</ListItem>
        </Link>

        <Link
          onClick={() => props.onClose(false)}
          style={{
            textDecoration: "none",
            color: "white",
          }}
          to="/detail/2"
        >
          <ListItem button>Details</ListItem>
        </Link>
      </List>
    </Drawer>
  );
};

export default SideDrawer;
