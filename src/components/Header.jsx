import React, { useState, useEffect } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import MenuIcon from "@material-ui/icons/Menu";
import IconButton from "@material-ui/core/IconButton";
import SideDrawer from "./SideDrawer";
import Typography from "@material-ui/core/Typography";

const Header = () => {
  const [header, showHeader] = useState(false);
  const [drawerOpen, setDrawerOpen] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
  }, []);

  const handleScroll = () => {
    if (window.scrollY > 0) {
      showHeader(true);
    } else {
      showHeader(false);
    }
  };

  const toggleDrawer = (value) => {
    setDrawerOpen(value);
  };

  return (
    <AppBar
      position="fixed"
      style={{
        background: header ? "#ff9966" : "transparent",
        boxShadow: "none",
        padding: "10px",
      }}
    >
      <Toolbar>
        <div className="header_logo">
          <Typography variant="h6">The Veneue</Typography>
          <Typography variant="caption" display="block">
            Musical Events
          </Typography>
        </div>
        <IconButton
          aria-label="Menu"
          color="inherit"
          onClick={() => toggleDrawer(true)}
        >
          <MenuIcon />
        </IconButton>
        <SideDrawer
          open={drawerOpen}
          onClose={(value) => toggleDrawer(value)}
        />
      </Toolbar>
    </AppBar>
  );
};

export default Header;
