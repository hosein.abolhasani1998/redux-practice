export const getSongs = () => async (dispatch) => {
  try {
    const res = await fetch("/songs");
    const data = await res.json();

    dispatch({
      type: "GET_SONGS",
      payload: data,
    });
  } catch (err) {
    dispatch({
      type: "SONG_ERROR",
      payload: err.response.statusText,
    });
  }
};

export const addSong = (song) => async (dispatch) => {
  const res = await fetch("/songs", {
    method: "POST",
    body: JSON.stringify(song),
    headers: {
      "Content-Type": "application/json",
    },
  });
  song.id = Math.random().toString(36).substr(2, 5);

  const data = await res.json();
  dispatch({
    type: "ADD_SONG",
    payload: data,
  });
};

export const selectSong = (song) => {
  return {
    type: "SELECT_SONG",
    payload: song,
  };
};
