import { combineReducers } from "redux";

export const songsReducer = (songs = [], action) => {
  switch (action.type) {
    case "GET_SONGS":
      return action.payload;

    case "ADD_SONG":
      return [...songs, action.payload];
    default:
      return songs;
  }
};
const defaultSong = {
  id: 2,
  title: "Talk Of Nothing",
  duration: "4:35",
  url:
    "https://images.unsplash.com/photo-1517230878791-4d28214057c2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80",
  desc:
    "Music is sound that has been organized by using rhythm, melody or harmony. ... Tunes are made of notes that go up or down or stay on the same pitch. Music often has rhythm. Rhythm is the way the musical sounds and silences are put together in a sequence.",
};

export const selectedSongReducer = (selectedSong = defaultSong, action) => {
  switch (action.type) {
    case "SELECT_SONG":
      return action.payload;

    default:
      return selectedSong;
  }
};

export default combineReducers({
  songs: songsReducer,
  song: selectedSongReducer,
});
